---
- mr.1320
- mr.1324
- mr.1329
---

os: Rename threading functions to more clearly state that it both stops and
waits on the thread. Also add asserts to make sure primitives have ben
initialized.
