---
- mr.943
- mr.1326
---

d3d: Add D3D helpers used by various parts of Monado, mostly the D3D11 client
compositor.
